# Assignment SDET

**IMPORTANT:** this assignment reflects ~7 hours of my work

* Develop a test framework that tests the https://openweathermap.org/current OpenWeather API. Using the framework above ensure you have automated client API tests for the following queries:
  * Call current weather data for one location
    * By city name
    * By city ID
    * By geographic coordinates
    * By ZIP code
  * Call current weather data for several cities
    * Cities within a rectangle zone
    * Cities in circle
* Develop an automated UI test framework for testing https://openweathermap.org/ web application. Using the framework developed above, add the following test cases:
  * it can find cities by name and display the list of matches
  * it can display forecast for given city
  * it can display both temperature and precipitation forecast
* Ensure your tests are properly containerized or packaged in a way someone can execute them in any environment
  * Prefer the UI and Client tests are dockerized into separate containers
  * if using selenium or other tools, they are containerized and packaged properly for someone to run the tests.
  * Running the tests needs zero setup, everything needed should be packaged properly
  * Provide good documentation and may be a short design of your framework
  

Run python tests
sudo docker build -t pytest_docker:latest -f pytest.dockerfile .
sudo docker run pytest_docker:latest

Run cypress tests
sudo docker build -t cypress_docker:latest -f cypress.dockerfile .
sudo docker run cypress_docker:latest

Run cypress with gherkin (just locally)
npx cypress run --spec **/*.feature
