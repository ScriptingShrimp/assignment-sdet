#!/bin/bash

echo $PWD
pytest -v --junit-xml=/api_tests/results/api_tests.xml
cat entrypoint.sh
find / -name "api_tests.xml"