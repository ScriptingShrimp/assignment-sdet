#!/usr/bin/env python3
import copy
import requests

class GetWeatherBy:
    """encapsulation for openweathermap API calls"""
    
    def __init__(self, apiKey):
        self.baseUrl = "https://api.openweathermap.org/data/2.5"
        self.weatherApiUrl = self.baseUrl + "/weather"
        self.boxApiUrl = self.baseUrl + "/box/city"
        self.findApiUrl = self.baseUrl + "/find"
        self.payload = {
            "appid": apiKey
        }

    def cityName(self, query: str):
        """ find city by city name;
            query: q={city name},{state code},{country code}"""
        params = copy.deepcopy(self.payload)
        params["q"] = query
        r = requests.get(self.weatherApiUrl, params=params)
        return r
        
    def cityID(self, cityId: str):
        """ find city by city ID;
            query: id={city id}"""
        params = copy.deepcopy(self.payload)
        params["id"] = cityId  
        r = requests.get(self.weatherApiUrl, params=params)
        return r

    def cityGps(self, lat: str, lon: str):
        """ find city by city GPS coordinates;
            query: lat={lat}&lon={lon}"""
        params = copy.deepcopy(self.payload)
        params["lat"] = lat
        params["lon"] = lon  
        r = requests.get(self.weatherApiUrl, params=params)
        return r 

    def cityZip(self, zip: str):
        """ find city by city ZIP code;
            query: zip={zip code},{country code}"""
        params = copy.deepcopy(self.payload)
        params["zip"] = zip
        r = requests.get(self.weatherApiUrl, params=params)
        return r 
    
    def citiesRectangle(self, rectangle: str):
        """ find cities within the defined rectangle;
            query: bbox={bbox}"""
        params = copy.deepcopy(self.payload)
        params["bbox"] = rectangle
        r = requests.get(self.boxApiUrl, params=params)
        return r 

    def citiesCircle(self, lat: str, lon: str, cnt: str):
        """ find cities within definite circle;
            query: lat={lat}&lon={lon}&cnt={cnt}"""
        params = copy.deepcopy(self.payload)
        params["lat"] = lat
        params["lon"] = lon
        params["cnt"] = cnt
        r = requests.get(self.findApiUrl, params=params)
        return r 

apiKey = "25e0cf69b962456567f6391d6e9882c0"

owm = GetWeatherBy(apiKey = apiKey)

# owm.cityName(query="Prague") 
# owm.cityName(query="London,UK")
# owm.cityName(query="Prague,203")
# owm.cityName(query="cz,brq")
# owm.cityName(query="Prague")

# owm.cityID(cityId="3067696")  
# owm.cityGps(lat="50.088039", lon="14.42076")
# owm.cityZip(zip="94040,us") 

# owm.citiesRectangle(rectangle="12,32,15,37,10")
# owm.citiesCircle(lat="55.5", lon="37.5", cnt="10")
