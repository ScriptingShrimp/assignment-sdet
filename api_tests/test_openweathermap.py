# Arrange
# Act
# Assert
from pytest_schema import schema, And, Enum, Optional, Or, Regex
from openweathermap import GetWeatherBy
from collections.abc import Iterable


apiKey = "25e0cf69b962456567f6391d6e9882c0"
owm = GetWeatherBy(apiKey = apiKey)

def test_cityName():
    """ Test response shape and validates its data 
        I used pytest_schema library, but schema or voluptuous is much more robust"""
    r = owm.cityName(query="Prague")
    data = r.json()
    
    cityNameData = {
        "coord":{
            "lon": Or(int, float),
            "lat": Or(int, float)
        },
        "weather":[
            {
                "id": int,
                "main": str,
                "description": str,
                "icon": Or(str, None)
            }
        ],
        "base":"stations",
        "main":{
            "temp": Or(int, float),
            "feels_like": Or(int, float),
            "temp_min": Or(int, float),
            "temp_max": Or(int, float),
            "pressure": Or(int, float),
            "humidity": Or(int, float)
        },
        "visibility": Or(int, float),
        "wind":{
            "speed": Or(int, float),
            "deg": Or(int, float)
        },
        "clouds":{
            "all": Or(int, float)
        },
        "dt": int,
        "sys":{
            "type": int,
            "id": int,
            "country": str,
            "sunrise": int,
            "sunset": int
        },
        "timezone": int,
        "id": int,
        "name": str,
        "cod": int
    }

    assert r.status_code  == 200
    assert schema(cityNameData) == data

def test_cityID():
    """ Test that ID 3067696 is equal to Prague """
    r = owm.cityID(cityId="3067696")
    data = r.json()
    assert r.status_code  == 200
    assert data["name"] == "Prague"

def test_cityGps():
    """ Test that lat="50.088039", lon="14.42076" is equal to Prague """
    r = owm.cityGps(lat="50.088039", lon="14.42076")
    data = r.json()
    assert data != None
    assert r.status_code  == 200
    assert data["name"] == "Prague"

def test_cityZip():
    """ Test that zip="94040,us" is equal to gps coordinates """
    r = owm.cityZip(zip="94040,us")
    data = r.json()
    assert r.status_code  == 200
    assert data["coord"] == {"lon":-122.088,"lat":37.3855}

def test_citiesRectangle():
    """ Test that rectangle="12,32,15,37,10" is iterable list """
    r = owm.citiesRectangle(rectangle="12,32,15,37,10")
    data = r.json()
    iterable = []
    assert r.headers["Content-Type"] == "application/json; charset=utf-8"
    assert r.status_code  == 200
    assert isinstance(data, Iterable)

def test_citiesCircle():
    """ Test that lat="55.5", lon="37.5", cnt="10" is iterable list """
    r = owm.citiesCircle(lat="55.5", lon="37.5", cnt="10")
    data = r.json()
    iterable = []
    assert r.headers["Content-Type"] == "application/json; charset=utf-8"
    assert r.status_code  == 200
    assert isinstance(data, Iterable)

# headers (app/json)
# response is not None (empty payload)
# smoke test before fun / prepare block?
# non-functional - measure latency, scale one call
# assert exceptions and catch them in imported class
# owm.cityZip(zip="94040,us") coordinates are equal