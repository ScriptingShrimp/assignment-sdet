const timeout = 30000
let responseArray = []
let lastResponse
const moment = require('moment')

describe(
  '/ (Home Page)',
  () => {
    let page
    beforeAll(async () => {
      page = await global.__BROWSER__.newPage()
      await page.setViewport({
        width: 1200,
        height: 800,
        deviceScaleFactor: 1,
      });
      await page.setUserAgent('Automated Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:69.0) Gecko/20100101 Firefox/69.0 OrionTests:true')

      await page.goto('https://o1.gpsguard.eu/')

      it('should proceed to login', async () => {
        let text = await page.evaluate(() => document.body.textContent)
        expect(text).toContain('Log in to the system')
      })

    }, timeout)

    afterAll(async () => {
      await page.close()
    })

    it('fill username and password, check form', async () => {

      await page.type('#UserName', '****');
      await page.type('#Password', '****');');

      let text = await page.evaluate(() => document.body.textContent)
      expect(text).toContain('Password')
    })

    it('submit form, navigates to app, checks 200 http reponse', async () => {

      const [response] = await Promise.all([
        page.waitForNavigation({ timeout: "15000", "waituntil": "networkidle2" }),
        page.click('input[type="submit"]'),
      ]);

      expect(JSON.stringify(response._status)).toContain(200)
    })

    it('wait for grid elements', async () => {

      const gridElements = await page.waitFor("div[data-vehicle-code]", timeout)
      expect.anything(gridElements)
    })

    it('read grid elements', async () => {
      const feedHandle = await page.$$("div[data-vehicle-code]")

      let times = await processArray(feedHandle)
      async function processArray(array) {
        for (const item of array) {
          let timeValues = await item.$$eval('span[data-current-zone]', nodes => nodes.map(n => n.getAttribute("data-current-zone")))
          let isValid = moment(await timeValues[0]).isValid()

          if (isValid && timeValues.length != 0) {
            responseArray.push(timeValues)
          } else {
            console.log('unparsable date in vehicle grid')
          }
        }
      }

      expect(responseArray.length).toBeGreaterThan(0)
    })

    it('convert and validate grid elements', async () => {
      let sortedArray = []
      for (const item of responseArray) {
        let unixTime 
        unixTime = item.toString()
        unixTime = moment(unixTime, "YYYY-MM-DDTHH:mm").unix()
        sortedArray.push(unixTime)
      }

      sortedArray.sort()
      lastResponse = sortedArray.pop()
      
      
      expect(lastResponse).toBeTruthy()
    })

    it('chcek last response is less than 1 hour', async () => {
      lastResponse = lastResponse + 3600
      let timeNow = moment().unix()

      if ((typeof(lastResponse) === 'number')) {
        expect(lastResponse).toBeGreaterThan(timeNow)
      } else {
        throw Error('unparsable time from webapp, something is wrong with dates')
      }
    })

  },
  timeout
)
