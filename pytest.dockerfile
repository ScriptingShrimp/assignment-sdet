# Get the python 3.8 base docker image
FROM python:3.8

# Layers!
COPY api_tests /api_tests
WORKDIR /api_tests

# RUN pipenv install — system — deploy
# Install all the dependencies from your lock file directly into the # container
RUN pip install --no-cache-dir -r requirements.txt

CMD ["/api_tests/entrypoint.sh"]

